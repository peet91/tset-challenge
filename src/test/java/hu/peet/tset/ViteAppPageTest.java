package hu.peet.tset;

import hu.peet.tset.base.TestContext;
import hu.peet.tset.util.BaseUtils;
import hu.peet.tset.util.Wait;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class ViteAppPageTest extends TestContext {

//    Testdata
//    Price components
//    Alloy surcharge: 2.15
//    Scrap surcharge: 3.14
//    Internal surcharge: 0.7658
//    External surcharge: 1
//    Storage surcharge: 0.3

    Map<String, String> testData;

    @BeforeClass
    public void initTestData() {
        testData = new HashMap<>();
        testData.put("Alloy surcharge", "2.15");
        testData.put("Scrap surcharge", "3.14");
        testData.put("Internal surcharge", "0.7658");
        testData.put("External surcharge", "1");
        testData.put("Storage surcharge", "0.3");
    }

    @Test(description = "1. Change Base Price value to 5")
    public void change_base_price_value_to_5() {
        viteAppPage.moveToElement(driver, driver.findElement(viteAppPage.getBasePriceWrapper()));

        WebElement basePrice = driver.findElement(viteAppPage.getBasePriceWrapper());

        Wait.until(() -> !BaseUtils.isChildElementAvailable(basePrice, viteAppPage.getEditButton()), 1000);

        basePrice.findElement(viteAppPage.getEditButton()).click();

        viteAppPage.editBasePrice(basePrice, 5.0);

        basePrice.findElement(viteAppPage.getSaveButton()).click();

        Assert.assertEquals(viteAppPage.getTotalPrice(driver), Double.valueOf(5.0));
    }

    @Test(description = "2. Add all price components from Testdata")
    public void add_all_price_components_from_testdata() {
        testData.entrySet().forEach(testDataItem -> {
            viteAppPage.addNewItem(driver, testDataItem.getKey(), testDataItem.getValue());
            testDataItem.setValue(String.valueOf(
                    BaseUtils.round(Double.valueOf(testDataItem.getValue()), 2)));
        });
    }

    @Test(description = "3. Remove price component: Internal surcharge")
    public void remove_price_component_internal_surcharge() {
        testData.entrySet().forEach(testDataItem -> {
            viteAppPage.addNewItem(driver, testDataItem.getKey(), testDataItem.getValue());
            testDataItem.setValue(String.valueOf(
                    BaseUtils.round(Double.valueOf(testDataItem.getValue()), 2)));
        });

        viteAppPage.removeItem(driver, "Internal surcharge");

        testData.remove("Internal surcharge");

        //A. Displayed sum shows correct sum
        validateSum();
    }

    @Test(description = "4. Edit price component: Storage surcharge")
    public void edit_price_component_storage_surcharge() {
        testData.entrySet().forEach(testDataItem -> {
            viteAppPage.addNewItem(driver, testDataItem.getKey(), testDataItem.getValue());
            testDataItem.setValue(String.valueOf(
                    BaseUtils.round(Double.valueOf(testDataItem.getValue()), 2)));
        });

        WebElement elementWithInvalidNewLabel = viteAppPage.editItem(driver, "Storage surcharge",
                "T", testData.get("Storage surcharge"));

        elementWithInvalidNewLabel.findElement(viteAppPage.getSaveButton()).click();

        //C. Label input validation
        viteAppPage.validateItem(driver, viteAppPage.getItemIndex(driver, "Storage surcharge"),
                "Storage surcharge", Double.valueOf(testData.get("Storage surcharge")));
    }

    @Test(description = "5. Edit price component: Scrap surcharge")
    public void edit_price_component_scrap_surcharge() {
        testData.entrySet().forEach(testDataItem -> {
            viteAppPage.addNewItem(driver, testDataItem.getKey(), testDataItem.getValue());
            testDataItem.setValue(String.valueOf(
                    BaseUtils.round(Double.valueOf(testDataItem.getValue()), 2)));
        });

        WebElement elementWithInvalidNewValue = viteAppPage.editItem(driver, "Scrap surcharge",
                "Scrap surcharge", "-2.15");

        elementWithInvalidNewValue.findElement(viteAppPage.getSaveButton()).click();

        //D. Value input validation
        viteAppPage.validateItem(driver, viteAppPage.getItemIndex(driver, "Scrap surcharge"),
                "Scrap surcharge", Double.valueOf(testData.get("Scrap surcharge")));
    }

    @Test(description = "6. Edit price component: Alloy surcharge")
    public void edit_price_component_alloy_surcharge() {
        testData.entrySet().forEach(testDataItem -> {
            viteAppPage.addNewItem(driver, testDataItem.getKey(), testDataItem.getValue());
            testDataItem.setValue(String.valueOf(
                    BaseUtils.round(Double.valueOf(testDataItem.getValue()), 2)));
        });

        testData.put("Alloy surcharge","1.79");

        WebElement elementWithInvalidNewValue = viteAppPage.editItem(driver, "Alloy surcharge",
                "Alloy surcharge", testData.get("Alloy surcharge"));

        elementWithInvalidNewValue.findElement(viteAppPage.getSaveButton()).click();

        viteAppPage.validateItem(driver, viteAppPage.getItemIndex(driver, "Alloy surcharge"),
                "Alloy surcharge", Double.valueOf(testData.get("Alloy surcharge")));

        //A. Displayed sum shows correct sum
        validateSum();
    }

    //A. Displayed sum shows correct sum
    private void validateSum() {
        Double expectedSum = 0.0;

        WebElement basePrice = driver.findElement(viteAppPage.getBasePriceWrapper());
        expectedSum += Double.valueOf(basePrice.findElement(viteAppPage.getItemValue()).getText().trim());

        expectedSum += testData.values().stream().mapToDouble(Double::valueOf).sum();

        //not sure if this assertion is enough, there's a better solution for comparing doubles
        Assert.assertEquals(viteAppPage.getTotalPrice(driver), expectedSum);
    }

}
