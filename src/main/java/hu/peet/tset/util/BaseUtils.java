package hu.peet.tset.util;

import org.openqa.selenium.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

public class BaseUtils {

    public static boolean isWebElementAvailable(WebDriver driver, By selector) {
        try {
            driver.findElement(selector);
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public static boolean isChildElementAvailable(WebElement element, By selector) {
        try {
            element.findElement(selector);
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public static List<String> getAttributeList(WebElement element, String attribute) {
        String classString = element.getAttribute(attribute);
        String[] classes = classString.split(" ");

        return Arrays.asList(classes);
    }

    public static void javascriptClick(WebDriver driver, WebElement element) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click();", element);
    }

    public static void scrollToWebElement(WebDriver driver, WebElement element) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView({" +
                "behavior: 'auto'," +
                "block: 'center'," +
                "inline: 'center' });", element);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
