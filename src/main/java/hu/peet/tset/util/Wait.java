package hu.peet.tset.util;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class Wait {
    public static void delay(long delay, TimeUnit timeUnit) {
        try {
            timeUnit.sleep(delay);
        } catch (InterruptedException ignored) {
            throw new RuntimeException("Unable to wait for the expected time");
        }
    }

    public static void until(Supplier<Boolean> supplier, int timeout) {
        int delay = 500;
        while (supplier.get() && timeout > 0) {
            delay(delay, TimeUnit.MILLISECONDS);
            timeout -= delay;
        }
    }

    public static void untilByDelay(Supplier<Boolean> supplier, int timeout, int delay) {
        while (supplier.get() && timeout > 0) {
            delay(delay, TimeUnit.MILLISECONDS);
            timeout -= delay;
        }
    }
}
