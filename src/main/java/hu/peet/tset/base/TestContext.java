package hu.peet.tset.base;

import hu.peet.tset.pages.ViteAppPage;
import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

@Getter
public class TestContext {
    public static String frontendUrl = System.getProperty("frontend.url");
    public static String frontendPort = System.getProperty("frontend.port");

    protected WebDriver driver;

    protected ViteAppPage viteAppPage;

    @BeforeMethod
    public void startTestSession() {
        System.out.println("--------------INIT TEST CONTEXT--------------");
        System.out.println("frontendUrl: " + frontendUrl);
        System.out.println("frontendPort: " + frontendPort);
        System.out.println("-------------/INIT TEST CONTEXT--------------\n");

        initFrontendUrl();
        Proxy proxy = new Proxy();
        proxy.setHttpProxy("57.56.133.0:3128");

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("proxy", proxy);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");

        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        driver = new ChromeDriver(options);
        viteAppPage = new ViteAppPage();

        try {
            driver.get(viteAppPage.getUrl());
            driver.findElement(By.cssSelector("div#app"));
        } catch (NoSuchElementException e) {
            driver.close();
            driver.quit();
            System.exit(1);
        }
    }

    @AfterMethod
    protected void close(ITestResult result) {
        try {
            driver.quit();
        } catch (WebDriverException e) {
            System.err.println("Couldn't properly close WebDriver!");
            System.err.println(e.getMessage());
        }
    }

    private void initFrontendUrl() {
        if (frontendUrl == null || frontendPort == null) {
            if (frontendUrl == null) {
                frontendUrl = "http://localhost";
            }

            if (frontendPort == null) frontendPort = "3000";

            System.out.println("Environment variable \"frontend.url\" or \"frontend.port\" isn't set, trying with "
                    + frontendUrl + ":" + frontendPort + "!");
        }
    }
}

