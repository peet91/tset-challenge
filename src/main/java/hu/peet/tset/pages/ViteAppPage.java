package hu.peet.tset.pages;

import hu.peet.tset.base.TestContext;
import hu.peet.tset.util.BaseUtils;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.util.List;

@Getter
public class ViteAppPage {
    private final String url = TestContext.frontendUrl + ":" + TestContext.frontendPort;

    private By homePageWrapper = By.cssSelector("div#app");

    private By totalPriceValue = By.cssSelector("div.text-right.mr-16.pr-2 > span");

    private By priceItems = By.cssSelector("ul.list-unstyled > div");

    private By basePriceWrapper = By.cssSelector("div#BasePrice");

    private By saveButton = By.cssSelector("span[id$='check-icon']");
    private By editButton = By.cssSelector("span[id$='edit-icon']");
    private By deleteButton = By.cssSelector("span[id$='thrash-icon']");

    private By newItemRow = By.cssSelector("div#Ghost");

    private By itemLabelInput = By.cssSelector("input[id$='label-input']");
    private By itemValueInput = By.cssSelector("input[id$='value-input']");

    private By itemLabel = By.cssSelector("div:nth-child(2) > span");
    private By itemValue = By.cssSelector("div.text-right");

    public Double getTotalPrice(WebDriver driver) {
        return Double.valueOf(driver.findElement(totalPriceValue).getText());
    }

    public List<WebElement> getPriceItemList(WebDriver driver) {
        return driver.findElements(priceItems);
    }

    public int getItemIndex(WebDriver driver, String label) {
        List<WebElement> priceItemList = getPriceItemList(driver);
        for (int i = 0; i < priceItemList.size(); ++i) {
            if (BaseUtils.isChildElementAvailable(priceItemList.get(i), itemLabel) &&
                    label.equals(priceItemList.get(i).findElement(itemLabel).getText().trim())) {
                return i;
            }
        }
        throw new RuntimeException("No price element found with label: " + label);
    }

    public void moveToElement(WebDriver driver, WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).build().perform();
    }

    public void editBasePrice(WebElement basePrice, Double value) {
        basePrice.findElement(itemValueInput).clear();
        basePrice.findElement(itemValueInput).sendKeys(String.valueOf(value));
    }

    public void fillItem(WebElement item, String label, String value) {
        //this was nasty, because we have to click into the field, or i
        item.findElement(itemLabelInput).click();
        item.findElement(itemLabelInput).clear();
        item.findElement(itemLabelInput).sendKeys(label);
        item.findElement(itemValueInput).clear();
        item.findElement(itemValueInput).sendKeys(value);
    }

    public void addNewItem(WebDriver driver, String label, String value) {
        WebElement newItem = driver.findElement(newItemRow);
        fillItem(newItem, label, value);
        newItem.findElement(saveButton).click();

        //B. Displayed values of price components are rounded correctly
        Double roundedValue = BaseUtils.round(Double.valueOf(value), 2);
        validateItem(driver, getPriceItemList(driver).size() - 2, label, roundedValue);
    }

    public void validateItem(WebDriver driver, int itemIndex, String label, Double value) {
        WebElement item = getPriceItemList(driver).get(itemIndex);
        Assert.assertEquals(item.findElement(itemLabel).getText().trim(), label);
        Assert.assertEquals(Double.valueOf(item.findElement(itemValue).getText().trim()), value);
    }

    public void removeItem(WebDriver driver, String label) {
        List<WebElement> priceItemElements = getPriceItemList(driver);

        WebElement elementToDelete = priceItemElements.stream().filter(element ->
                label.equals(element.findElement(itemLabel).getText().trim())).findFirst().orElseThrow(() ->
                new RuntimeException("No price element found with label: " + label));

        moveToElement(driver, elementToDelete);
        elementToDelete.findElement(deleteButton).click();

        priceItemElements = getPriceItemList(driver);

        //we need to filter out the "new row"
        Assert.assertTrue(priceItemElements.stream().noneMatch(element ->
                BaseUtils.isChildElementAvailable(element, itemLabel) &&
                        label.equals(element.findElement(itemLabel).getText().trim())));
    }

    public WebElement editItem(WebDriver driver, String oldLabel, String newLabel, String value) {
        List<WebElement> priceItemElements = getPriceItemList(driver);

        WebElement elementToEdit = priceItemElements.stream().filter(element ->
                oldLabel.equals(element.findElement(itemLabel).getText().trim())).findFirst().orElseThrow(() ->
                new RuntimeException("No price element found with label: " + oldLabel));

        moveToElement(driver, elementToEdit);
        elementToEdit.findElement(editButton).click();

        fillItem(elementToEdit, newLabel, value);

        return elementToEdit;
    }
}
